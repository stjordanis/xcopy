# XCopy

Copies files and directory trees.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## XCOPY.LSM

<table>
<tr><td>title</td><td>XCopy</td></tr>
<tr><td>version</td><td>1.4a</td></tr>
<tr><td>entered&nbsp;date</td><td>2007-09-22</td></tr>
<tr><td>description</td><td>Copies files and directory trees.</td></tr>
<tr><td>keywords</td><td>freedos, copy, xcopy, kitten</td></tr>
<tr><td>author</td><td>Rene Ableidinger &lt;rene.ableidinger@gmx.at&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Rene Ableidinger &lt;rene.ableidinger@gmx.at&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/xcopy</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
